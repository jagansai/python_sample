import json
from typing import List
from typing import Set
from functools import reduce
from operator import mul
import csv
from random import random, randint

from WordFinder.demo import find_words
from calculator.demo import calculator_simple
from dates.date_tutorials import print_dates
from error_handling.error_handling import error_handling_demo

from getcombinations import print_word_combinations, get_combinations
from getcombinations import print_word_permutations, get_permutations

from knapsack.demo import match_total_test
from Tries.demo import TrieDemo


class Args(object):
    def __init__(self, input_file: str, header_format: str, output_format: str = '', show_help: bool = False,
                 dry_run: bool = False):
        self.showHelp = show_help
        self.dryRun = dry_run
        self.inputFile = input_file
        self.headerFormat = header_format
        self.outputFormat = output_format

    def __str__(self):
        return json.dumps(self.__dict__).replace(', ', ', \n')


def getpairs(arr: List[int], sum: int):
    lenofarr = len(arr)
    for i in range(0, lenofarr):
        for j in range(i + 1, lenofarr):
            if arr[i] + arr[j] == sum:
                yield arr[i], arr[j]


def sayHello(name: str, age: int = 34) -> str:
    return f'hello {name} !!! And you are {age} years old'


def is_odd_or_even(num):
    return "Even" if num % 2 == 0 else "Odd"


def print_odd_even(range_of_num: int, random_num_range: int):
    print(f"Printing {range_of_num} of odd / evens between a random of 1 to {random_num_range}")
    for _ in range(1, range_of_num):
        num = randint(1, random_num_range)
        print(f"{num} is {is_odd_or_even(num)}")


def foo(required, *args, **kwargs):
    print(required)
    if args:
        print(args)
    if kwargs:
        print(kwargs)


def bar(required, *args, **kwargs):
    foo(required, args, kwargs)


def baz(**kwargs):
    print(**kwargs)


def classic_fibonacci(limit):
    nums = []
    current, nxt = 0, 1
    while current < limit:
        current, nxt = nxt, nxt + current
        nums.append(current)

    return nums


def generator_fibonacci():
    current, nxt = 0, 1
    while True:
        current, nxt = nxt, nxt + current
        print(f'cur:{current}, next:{nxt}')
        yield current


def generator_evn(numbers):
    for num in numbers:
        if num % 2 == 0:
            yield num


def even_fib():
    for num in generator_evn(generator_fibonacci()):
        yield num


def product_of_other_nums_in_arr_1(arr: List[int]):
    """This method gets the product of all other numbers in the array except the number in the index.
       Then returns the new array.
     """
    product = reduce(mul, arr)
    return [int(product / x) for x in arr]


def product_of_other_nums_in_arr_2(arr: List[int]):
    """This method gets the product of all other numbers in the array except the number in the index.
       Then returns the new array.
     """
    L = len(arr)
    r = [1] * L
    after = before = 1
    for i in range(1, L):
        before *= arr[i - 1]
        r[i] = before
    for i in range(L - 2, -1, -1):
        after *= arr[i + 1]
        r[i] *= after
    return r


def find_partition(arr: List[int]):
    """To find the number at the partition where the sum of left side of the array is same as the sum of the right
    side of the array
    :rtype: int"""
    size = len(arr)
    right_sum, left_sum = 0, 0

    for i in range(1, size):
        right_sum += arr[i]

    i, j = 0, 1
    while j < size:
        right_sum -= arr[j]
        left_sum += arr[i]
        if right_sum == left_sum:
            return arr[i + 1]
        i += 1
        j += 1
    return -1


def find_first_index_of_target(arr: List[int], target: int) -> int:
    """To find the first index of the target in the array of sorted values."""
    lo, hi = 1, len(arr)
    while lo <= hi:
        mid = int(lo + (hi - lo) / 2)
        if arr[mid] == target and (mid == 0 or arr[mid - 1] < target):
            return mid
        elif arr[mid] < target:
            lo = mid + 1
        else:
            hi = mid - 1
    return -1


def reverse_words(line: str) -> str:
    # reverse only alnum characters.
    i, j = 0, len(line) - 1
    while i <= j:
        if line[i].isalnum() and line[j].isalnum():
            line[i], line[j] = line[j], line[i]
            i += 1
            j -= 1
        elif line[i].isalnum():
            j -= i
        elif line[j].isalnum():
            i += 1

    print(f'reversed:{line}')
    return line


def predict_highest_score(word: str):
    def check_letter_in_cache(combination: str, letter_len_cache):
        return combination[0] in letter_len_cache and len(combination) in letter_len_cache[combination[0]]

    path_to_dict = "D:/code/words.txt"
    words_cache = set({})
    letter_len_cache = {}
    with open(path_to_dict, 'r') as dict_file:
        for line in dict_file:
            words_cache.add(line.strip('\n').upper())
            letter_len_cache.setdefault(line[0], set({})).add(len(line))

    print(f'build the dictionary of words:{len(words_cache)}')
    # Now get the combination of the word that's passed as argument and find them in the dictionary.
    for c in get_permutations(word):
        combination = ''.join(c)
        if check_letter_in_cache(combination, letter_len_cache):
            if combination in words_cache:
                print(combination)
        else:
            print(f'{combination} not in dicitionary')


def read_csv():
    with open("./sample.csv", 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        print(','.join(csv_reader.fieldnames))
        for row in csv_reader:
            print(f"{row['name']}, {row['age']}")


def return_after_5_secs(message, sleep_time):
    from time import sleep
    sleep(sleep_time)
    message = f"slept from {sleep_time} secs"
    print(message)
    return message


def test_futures():
    from concurrent.futures import ThreadPoolExecutor, thread
    from concurrent import futures

    from random import randint
    with ThreadPoolExecutor() as pool:
        future_list = []
        for i in range(1, 10):
            future_list.append(pool.submit(return_after_5_secs, "hello", i))

        for f in futures.as_completed(future_list):
            if f.done() and f.result():
                print(f.result())
                break
        pool._threads.clear()
        thread._threads_queues.clear()


if __name__ == '__main__':
    # print(sayHello('neeraja'))
    # ulimit = 10000
    # arr = []
    # for i in range(1, ulimit):
    #     arr.append(random.randint(-10, 50))

    # for i in arr:
    #     print(i)

    # for p in getPairs(arr, 55):
    #     print(p)

    # print([x for x in range(1, 1000)])

    # names:List[str] = ['jagan','neeraja','kartik','saketh']
    # csvnames = ','.join(names)
    # print(csvnames)
    # decorators.printNum(10)
    # Contextmanagers.openFileDemo()

    list_nums = [1, 2]
    xs = {1: 'one', 2: 'two'}
    foo('hello', 1, 2, 3, xs)

    print('classic')
    for num in classic_fibonacci(100):
        print(f'{num},')

    print('generator')
    for num in generator_fibonacci():
        print(f'{num},')
        if num > 100:
            break

    print('generat even fibonacci')
    for num in even_fib():
        print(num)
        if num > 100:
            break

    print("Printing the product of nums except the num in the index", end="   ---->\n")
    print(product_of_other_nums_in_arr_1([1, 2, 3, 4, 5]))
    print(product_of_other_nums_in_arr_2([1, 2, 3, 4, 5]))
    print(product_of_other_nums_in_arr_2([2, 3, 4, 5, 6, 7]))
    print(f'To find the partition in [1, 4, 2, 5]: {find_partition([1, 4, 2, 5])}')
    print(f'To find the partition in [2, 3, 4, 1, 4, 5]:{find_partition([2, 3, 4, 1, 4, 5])}')
    print(f'To find the index of 1 in :[0, 0, 0, 0, 1, ,1, 1]:{find_first_index_of_target([0, 0, 0, 0, 0, 1, 1], 1)}')
    # print(reverse_words("this-is!good"))
    predict_highest_score("ABEBYZ")
    # print_word_permutations("ABC")
    for e in get_permutations('543'):
        if len(e) == 3:
            print(e)


    def map_to_string(x):
        return ''.join(x)


    def map_str_to_int(x: str):
        return int(x)


    def filter_of_size(x, sz: int):
        return len(list(x)) == sz


    print(min(map(lambda x: int(''.join(x)), filter(lambda x: len(list(x)) == 3, get_permutations('543')))))

    print(min(map(map_str_to_int, map(map_to_string, filter(lambda x: filter_of_size(x, 3), get_permutations('543'))))))

    # read csv files.
    read_csv()
    print_odd_even(10, 1000)

    # print dates...
    print_dates()

    # Demo error handling.
    error_handling_demo()
    for i in range(6):
        find_words("D:\\code\\python_code\\python_sample\\master.txt")

    # test_futures()

    #match_total_test(num_items_in_list=20, items_between=(-20, 100), total_between=(0, 100))

   # calculator_simple()
    #TrieDemo()