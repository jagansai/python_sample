import requests
import json

from datetime import datetime
from dates.date_tutorials import add_days, prev_days, today, yesterday


def calling_api_demo(to_print):
    this_day = int((today() - datetime(1970, 1, 1)).total_seconds())
    previous_day = int((prev_days(2) - datetime(1970, 1, 1)).total_seconds())
    pagesize = 10
    lang = "python"
    url = f"https://api.stackexchange.com/2.2/questions?page=1&pagesize={pagesize}&fromdate={previous_day}&todate={this_day}&order=desc&sort=activity&tagged={lang}&site=stackoverflow"

    response = requests.get(url=url)
    response.raise_for_status()
    results = response.json()

    if to_print:
        print(json.dumps(results, indent=4))
    return results
