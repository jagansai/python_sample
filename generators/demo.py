from random import choice


def my_generator():
    print("multiple yield statements")
    yield 'a'
    yield 'b'
    yield 'c'


def bidirectional_generator():
    print("Bidirectional generator")
    SENTENCES = [
        "How are you?",
        "Fine, thank you!",
        "Nothing much",
        "Just chillin"
    ]

    def random_conversation():
        recv = yield 'Hi'
        while recv != 'Bye!':
            recv = yield choice(SENTENCES)

    g = random_conversation()
    print(g.send(None))
    while True:
        try:
            reply = g.send(input())
        except StopIteration:
            break
        print('>>> ' + reply)
    print('Conversation over!')


def generate_data(num_rows):
    from collections import namedtuple
    from random import randrange

    clientType = ["Individual", "Corporate"]
    indvidualRange = [1000, 100000]
    corporate = [1000000, 5000000]

    rate = namedtuple("rate", ["base", "wanted", "time"])
    rates = []
    with open("D:\\code\\python_code\\python_sample\\rates.csv") as f:
        for index, line in enumerate(f):
            if index == 0:
                continue
            base, wanted, time = str(line).strip("\n").split(",")
            rates.append(rate(base, wanted, time))

    with open("D:\\code\\java_code\\FxTest\\inputdata\\transactions_large.csv", "w") as f:
        f.write("BaseCurrency,WantedCurrency,AmountInBaseCurrency,ClientType,TransactionTime\n")
        for _ in range(0, num_rows):
            hours = randrange(6, 19)
            minutes = f"{randrange(0, 23):0>2}"
            time = f'{hours}.{minutes}'
            r = rates[randrange(0, len(rates))]
            if r.time >= time:
                c = clientType[randrange(0, 2)]
                if c == "Individual":
                    amount = randrange(indvidualRange[0], indvidualRange[1])
                else:
                    amount = randrange(corporate[0], corporate[1])
                f.write(f"{r.base},{r.wanted},{amount},{c},{r.time}\n")
            else:
                pass


generate_data(10_000_000)
