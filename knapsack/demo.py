# Find out which combinations of numbers in a set add up to a given total

from itertools import combinations
from typing import List
from random import randint

def get_combinations(list_of_items, size=2):
    for item in combinations(list_of_items, size):
        yield item


def match_total(total, list_of_items: List[int]):

    while list_of_items.count(total) > 0:
        yield (total)
        list_of_items.remove(total)

    if len(list_of_items) == 0:
        return

    list_of_items.sort()
    most_neg_number = list_of_items[0]
    if most_neg_number > 0:
        most_neg_number = 0

    # if there are no negative numbers, then remove all the numbers
    # that are greater than the total.
    if most_neg_number == 0:
        for num in [x for x in list_of_items if x > total]:
            list_of_items.remove(num)

    # if there are no negative and for the remaining items in the list
    # if the sum is less than the total , then no need to look further
    if most_neg_number == 0 and sum(list_of_items) < total:
        return

    # get the combinations for the remaining list of items
    # skip 1 since we are already remove subtotals for that match.
    for num in range(2, len(list_of_items) + 1):
        for c in get_combinations(list_of_items, num):
            if sum(c) == total:
                yield c

def match_total_test(num_items_in_list = 10, items_between=(0, 15), total_between=(0, 50), max_matches=-1):
    # generate 50 random numbers between -50 to 50

    list_of_items = []
    for num in range(1, num_items_in_list):
        list_of_items.append(randint(items_between[0], items_between[1]))

    total = randint(total_between[0], total_between[1])
    print(f"Finding matches for {total} in a list of items: {list_of_items}")
    for index, c in enumerate(match_total(total, list_of_items)):
        to_continue = False if (max_matches is not -1 and index + 1 > max_matches) else True
        if to_continue:
            print(c)
        else:
            break
