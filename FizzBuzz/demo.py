"""
Write in the programming language that you want, a program that displays on screen the numbers from 1 to 100,
replacing the multiples of 3 by the word “Fizz” and,
in turn, the multiples of  5 by the word “Buzz”.
For the numbers that, in time, are multiples of 3 and 5, use the word “FizzBuzz”.
"""


def fizzbuzz_demo(upto):
    i = 1
    while i <= upto:
        if i % 3 == 0 and i % 5 == 0:
            yield "FizzBuzz"
        elif i % 3 == 0:
            yield "Fizz"
        elif i % 5 == 0:
            yield "Buzz"
        else:
            yield i
        i += 1
