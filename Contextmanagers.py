# This file demos contextmanagers in python.

# this is a contextmanager for a file open

from contextlib import contextmanager


@contextmanager
def openFile(filename):
    global f
    try:
        f = open(filename, "r")
        yield f
    finally:
        if f:
            f.close()


class MyContextManager:
    """ This class is used as contextmanager for file open and close."""

    def __init__(self, name):
        self.name = name

    def __enter__(self):
        print(f"{self.name} is being opened")
        self.file = open(self.name, "r")
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(f"Closing {self.name}")
        self.file.close()


class MyIndenter:
    def __init__(self):
        self.indent = 0

    def __enter__(self):
        self.indent += 1
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.indent -= 1

    def print(self, text):
        print('    ' * self.indent + text)


def openFileDemo():
    print("Demo using openFile function")
    with openFile("D:/code/sample.txt") as file:
        for line in file:
            print(line)

    print("Demo using MyContextManager class")
    with MyContextManager("D:/code/sample.txt") as file:
        for line in file:
            print(line)

    with MyIndenter() as indent:
        indent.print('hello')
        with indent:
            indent.print('hi')
            with indent:
                indent.print('bye')
                indent.print('bye')
            indent.print('hi')
        indent.print('hello')
