
class Trie(object):

    class TrieNode(object):
        """
        A class representing a node of a TRIE data structure.
        We declare multiple children of a node using a list.
        If this node represents an end of a key, then end_of_key would be True.
        """

        def __init__(self):
            self.children = [None] * 26  # number of alphabets.
            self.end_of_key = False

    def __init__(self):
        self.root = Trie.TrieNode()

    @staticmethod
    def _char_to_index(char) -> int:
        return ord(char) - ord('a')

    @staticmethod
    def _index_to_char(num) -> chr:
        return chr(num + ord('a'))

    def build_data(self, key):
        """
        Insert a key into Trie datastructure.
        :param key: Key to be inserted
        :return: None
        """

        key = key.lower()
        current_node = self.root
        for c in key:
            index = self._char_to_index(c)
            if current_node.children[index] is None:
                current_node.children[index] = Trie.TrieNode()

            current_node = current_node.children[index]
        current_node.end_of_key = True

    def contains(self, key):
        """
        Search for a key in Trie.
        :param key: key to search for
        :return: True if key is present. False otherwise.
        """

        key = key.lower()

        current_node = self.root
        for level in range(len(key)):
            index = self._char_to_index(key[level])

            if current_node.children[index] is None:
                return False

            current_node = current_node.children[index]
        return current_node is not None and current_node.end_of_key


def TrieDemo():
    from time import time, time_ns
    words = ["night", "young", "day", "man", "woman", "age"]

    start_time = time()
    trie = Trie()
    counter = 0
    with open ("D:\\code\\python_code\\python_sample\\popular.txt") as f:
        for word in f:
            trie.build_data(word.replace('\n', '').strip())
            counter += 1
    print(f"Took {time() - start_time}  to insert {counter} words into trie")

    format_dict = {True: "found", False: "not found"}
    print(f"Let's time the retrieval of {words}")

    for word in words:
        start_time = time()
        result = trie.contains(word)
        end_time = time()
        print(f"Took {int(end_time - start_time)*1000} ms to search for {word}. It is {format_dict[result]}")