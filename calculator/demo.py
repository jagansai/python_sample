import functools as ft

OPERATORS = "+", "-", "*", "/"
EXIT_COMMANDS = "exit", "quit"


def validate_input(func):
    def inner():
        i = func()
        try:
            i = float(i)
        except ValueError:
            pass
        if isinstance(i, float) or i in OPERATORS or i in EXIT_COMMANDS:
            return i
        else:
            return None

    return inner


@validate_input
def get_input():
    return input()


def input_loop():
    while True:
        i = get_input()
        if i in EXIT_COMMANDS:
            break
        if i is None:
            print(f"Please enter a number or an operator ({OPERATORS})")
            continue
        yield i


def process_input(state, update):
    state.append(update)

    if can_calculate(state):
        *_, i1, op, i2 = state
        result = calculate(i1, op, i2)
        state.append(result)
    return state


def process_input_simple():
    state = []
    while True:
        update = yield
        state.append(update)
        if can_calculate(state):
            *_, i1, op, i2 = state
            result = calculate(i1, op, i2)
            state.append(result)


def can_calculate(state):
    if (len(state)) < 3:
        return False
    *_, i1, op, i2 = state
    if isinstance(i1, float) and op in OPERATORS and isinstance(i2, float):
        return True
    else:
        return False


def calculate(i1, op, i2):
    result = 0.0
    if op == '+':
        result = i1 + i2
    elif op == '-':
        result = i1 - i2
    elif op == '*':
        result = i1 * i2
    elif op == '/':
        result = i1 / i2
    else:
        raise ValueError(f"Invalid operator: {op}")
    print(f"{i1} {op} {i2} = {result}")
    return result




def calculator_complicated():
    ft.reduce(process_input, input_loop(), [0])


def calculator_simple():
    g = process_input_simple()
    g.send(None)

    while True:
        i = get_input()
        if i in EXIT_COMMANDS:
            break
        elif i is None:
            print("Invalid input!")
            continue
        else:
            g.send(i)