from random import shuffle
import itertools as it

# Implement RandomIterator that iterates over elements in a random way.

class RandomIterator:

    def __init__(self, *elements):
        self._elements = list(elements)

    def __iter__(self):
        shuffle(self._elements)
        self._cursor = 0
        return self

    def __next__(self):
        if self._cursor >= len(self._elements):
            raise StopIteration()
        e = self._elements[self._cursor]
        self._cursor += 1
        return e


def iterator_unpacking():
    animals = "Ant", "Whale", "Lizard", "Dog"

    print(animals)

    a1, a2, a3, a4 = animals
    print(a1, a2, a3, a4)

    first, *rest, last = animals
    print(f"first:{first}, second to last but one:{rest}, last:{last}")

    politician_and_party = {
        "Modi": "BJP",
        "Rahul Gandhi": "Congress",
        "CBN": "TDP",
        "Amit Shah": "BJP",
        "Y S Jagan": "YSRCP"
    }

    (k1, v1), (k2, v2), (k3, v3), (k4, v4), (k5, v5) = politician_and_party.items()
    print(f"{k1} : {v1}")
    print(f"{k2} : {v2}")
    print(f"{k3} : {v3}")
    print(f"{k4} : {v4}")
    print(f"{k5} : {v5}")


def iterators_and_generators():
    t = "a", "b", "c"
    i = iter(t)
    while True:
        try:
            e = next(i)
        except StopIteration:
            break
        print(e)


def random_iterator():
    def print_values(it):
        for e in it:
            print(e)
        print('*****************')

    i = RandomIterator(1, 2, 3)
    print_values(i)
    print_values(i)


def iter_tools_goodies():
    print(f"takewhile....")
    for i in it.takewhile(lambda x: x < 10, it.count()):
        print(i)

    print(f"chain....")
    for i in it.chain('abc', [3,2,1], 'cba'):
        print(i)

    print(f"dropwhile...")
    for i in it.dropwhile(lambda x: x > 10, range(20, 0, -1)):
        print(i)

    print(f"groupby...")
    is_even = lambda x: x % 2 == 0

    for e, g in it.groupby(range(1, 10), is_even):
        print(e, list(g))

    for e, g in it.groupby(sorted(range(1, 11), key=is_even), key=is_even):
        print(e, list(g))


