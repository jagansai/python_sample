'''
 This is to test arrays and problems on arrays.
'''
from collections import deque
from typing import List


def left_rotation(arr: [], num_rotations):
    '''
    A left rotation operation on an array shifts each of the array's elements  unit to the left. For example, if  left rotations are performed on array [1,2,3,4,5], then the array would become [3,4,5,1,2].

    Given an array  of  integers and a number, , perform  left rotations on the array. Return the updated array to be printed as a single line of space-separated integers.
    '''

    queue = deque(arr)
    while num_rotations:
        queue.append(queue.popleft())
        num_rotations -= 1
    return queue


def right_rotation(arr: [], num_rotations):
    queue = deque(arr)
    while num_rotations:
        queue.insert(0, queue.pop())
        num_rotations -= 1
    return queue


def new_year_chaos(arr: []):
    bribes = 0
    for index, v in enumerate(arr, start=1):
        if index < v and v <= (index + 2):
            bribes += (v - index)
        elif index < v and v >= (index + 2):
            print("Too chaotic")
            break
    else:
        print(bribes)


def get_sum_of_sub_array(arr: List[int], target: int):
    from collections import namedtuple
    FromTo = namedtuple("FromTo", {"start", "end"})  # Define namedtuple that is yielded from get_combinations()

    def get_combinations():
        '''
          This method returns the sub-arrays to the array
        '''
        for i in range(1, len(arr) + 1):
            for index, _ in enumerate(range(len(arr))):
                if index + i > len(arr):
                    continue
                yield FromTo(start=index, end=index + i)

    for from_to in get_combinations():
        sub_arr = arr[from_to.start: from_to.end]
        if sum(sub_arr) >= target:
            return len(sub_arr)  # the sub-arrays are in ascending of length. The next item will be bigger / equal
            # length.

    return -1

def get_sum_of_sub_array_follow_up(arr: List[int], target: int):
    from sys import maxsize
    curr_min = maxsize

    def check_for_sum_at_least(sub_arr: List[List[int]]):
        result = filter(lambda x : sum(x) >= target, sub_arr)
        return len(min(result, key=lambda x: len(x), default=[]))


    if not arr:
        return -1
    sub_arr = []
    arr_till_now = []
    prev_val = None
    index = 0
    len_of_arr = len(arr)
    while index < len_of_arr:
        num = arr[index]
        arr_till_now.append(num)
        sub_arr.append(arr_till_now.copy()) 
        sub_arr.append(arr[index-1:]) # copy from previous index to end of the array.
        if prev_val:
            sub_arr.append([prev_val, num]) # Array of previous value and current value.
        prev_val = num
        result = check_for_sum_at_least(sub_arr)
        if result == 0:
            sub_arr.clear()
        elif result < curr_min:
            curr_min = result
        
        # if cur_min is 1, then that's the result we are looking for.
        # we can return from here.
        if curr_min == 1:
            return 1
        index += 1
    
    return -1 if curr_min == maxsize else curr_min
    
def get_max_sub_array(arr):
    """
    High Level Description:
    Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
    Time Complexity:
    O(n)
    """
    if not arr:
        return 0
    
    cur_sum = max_sum = arr[0]
    for num in arr[1:]:
        cur_sum = max(num, cur_sum + num)
        max_sum = max(max_sum, cur_sum)
    return max_sum






def test_custom_sort(arr: []):
    class Sorter:
        def __init__(self, a):
            self.arr = a

        def compare(self, x):
            return False, self.arr.index(x) if x % 2 == 0 else True, self.arr.index(x)

    s = Sorter(arr.copy())
    arr.sort(key=s.compare)
    print(arr)


print(' '.join(map(str, left_rotation([1, 2, 3, 4, 5], 2))))
print(' '.join(map(str, right_rotation([1, 2, 3, 4, 5], 2))))

print(new_year_chaos([2, 1, 5, 3, 4]))
print(new_year_chaos([2, 5, 1, 3, 4]))
print(new_year_chaos([1, 2, 5, 3, 7, 8, 6, 4]))

print(get_sum_of_sub_array([1, 2, 3, 4], 7))
print(get_sum_of_sub_array([1, 7, 6, 9, 2, 5, 3, 4], 22))
print(get_sum_of_sub_array([1, 2, 3, 4, 8, 5, 4], 10))
print(get_sum_of_sub_array([1, 2, 3, 4, 9], 55))
print(get_sum_of_sub_array([1, 7, 6, 9, 2, 5, 3, 4], 22))

test_custom_sort([1, 3, 4, 2, 5, 2])


# use dataclass to test this code.
from dataclasses import dataclass

@dataclass
class MaxSubArrayData:
    arr: List[int]

testdata = [
    MaxSubArrayData([1,2,3,4]),
    MaxSubArrayData([1, -1, 2, -3])
]

for td in testdata:
    #print(f"get_max_sub_array({td.arr}): {get_max_sub_array({td.arr})}")
    arr = td.arr
    print(f"get_max_sub_array({arr}): {get_max_sub_array(arr)}")

@dataclass
class MinLenSubArrayData:
    arr: List[int]
    target: int
    expected: int

testdata = [
    MinLenSubArrayData([1,2,3,4], 7, 2),
    MinLenSubArrayData([1,2,3,4,7], 7, 1),
    MinLenSubArrayData([1, 4, 2, 7],6, 1),
    MinLenSubArrayData([1, 2, 4, 3, 6, 7, 9], 10, 2),
    MinLenSubArrayData([1, 2, 4, 3, 6, 7, 9], 9, 1),
    MinLenSubArrayData([1, 2, 3, 4, 5, 6, 7], 9, 2),
    MinLenSubArrayData([1, 2, 3, 4, 5, 6, 7, 11], 18, 2),
    MinLenSubArrayData([1, 2, 3, 4, 5, 6, 7, 11], 999, -1)

]

pass_fail = True
pass_fail_map = {True: "All passed", False: "Not all passed"}
for td in testdata:
    arr = td.arr
    target = td.target
    output = get_sum_of_sub_array_follow_up(arr, target)
    print(f"get_sum_of_sub_array_follow_up({arr},{target}):{output}")
    pass_fail = pass_fail and (output == td.expected)

print(pass_fail_map[pass_fail])
