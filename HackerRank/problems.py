
from typing import List


def balanced_delimiters():
    """
    For this question, you will parse a string to determine if it contains only "balanced delimiters."
    A balanced delimiter starts with an opening character ((, [, {), ends with a matching closing character (), ], }
    respectively), and has only other matching delimiters in between. A balanced delimiter may contain any number of
    balanced delimiters.
     Examples
    The following are examples of balanced delimiter strings:

    ()[]{}
    ([{}])
    ([]{})
    The following are examples of invalid strings:

    ([)]
    ([]
    [])
    ([})
    """
    delimiter = input().strip('\n\"\'')

    while '()' in delimiter or '[]' in delimiter or '{}' in delimiter:
        delimiter = delimiter.replace('{}', '')
        delimiter = delimiter.replace('[]', '')
        delimiter = delimiter.replace('()', '')

    print(len(delimiter) == 0)


def uncoupled_integer():
    """
    Write a program that, given a list of integers as an argument to STDIN

    n1, n2, n3, ..
    Prints out the only uncoupled (unpaired) integer in the list to STDOUT.

    Example 1:

    Given the input

    1, 2, 3, 1, 2
    your program should output:

    3
    Example 2:

    Given the input

    1, 2, 3, 4, 5, 99, 1, 2, 3, 4, 5
    your program should output:

    99
    """

    from functools import reduce
    numbers = (int(x) for x in (input().strip("\n\"\'")).split(', '))
    print(reduce(lambda x, y: x ^ y, numbers))


def loopThroughFiles():
    from itertools import zip_longest
    from csv import DictReader
    eom_file = "D:\\code\\python_code\\python_sample\\HackerRank\\eom.csv"
    pke_swap_file = "D:\\code\\python_code\\python_sample\\HackerRank\\pke.csv"

    eom_dict = {}
    pke_swap_dict = {}

    def insert_into_dict(table_dict, row, **kwargs):
        if row is None:
            return
        else:
            for key, value in kwargs.items():
                table_dict[row[key]] = row[value]

    with open(eom_file) as eom_h:
        with open(pke_swap_file) as pke_h:
            eom_reader = DictReader(eom_h)
            pke_reader = DictReader(pke_h)
            for eom, pke in zip_longest(eom_reader, pke_reader):
                insert_into_dict(eom_dict, eom, **{"compid": "ips"})
                insert_into_dict(pke_swap_dict, pke, **{"compid": "ips"})

    for pke_compid, pke_ips in pke_swap_dict.items():
        if pke_compid in eom_dict:
            if len(eom_dict[pke_compid]) == 0:
                print(pke_compid, pke_ips)


def change_coins():
    def getWays(n, c):
        l = sorted(c)
        m = len(l)

        matrix = [[0 for i in range(m)] for i in range(n + 1)]

        matrix[0] = [1 for i in range(m)]

        for i in range(1, n + 1):
            for j in range(m):
                # Solutions with l[j]
                matrix[i][j] += matrix[i - l[j]][j] if i - l[j] >= 0 else 0
                # Solutions without l[j]
                matrix[i][j] += matrix[i][j - 1] if j >= 1 else 0

        return matrix[n][m - 1]

    nm = input().split()

    n = int(nm[0])
    c = list(map(int, input().rstrip().split()))

    # Print the number of ways of making change for 'n' units using coins having the values given by 'c'

    ways = getWays(n, c)
    print(ways)


# Skyline problem.

from collections import defaultdict, namedtuple
from heapq import heappop, heappush


class Building:
    def __init__(self, height):
        self.height = height
        self.finished = False

    def __lt__(self, other):
        # Reverse order by height, so that when we store buildings in
        # a heap, the first building is the highest.
        return other.height < self.height


# An event represents the buildings that start and end at a particular
# x-coordinate.
Event = namedtuple('Event', 'start end')


def skyline(buildings):
    """Given an iterable of buildings represented as triples (left, height,
    right), generate the co-ordinates of the skyline.

    >>> list(skyline([(1,9,3), (1,11,5), (2,6,7), (3,13,9), (12,7,16),
    ...               (14,3,25), (19,18,22), (23,13,29), (24,4,28)]))
    ... # doctest: +NORMALIZE_WHITESPACE
    [(1, 11), (3, 13), (9, 0), (12, 7), (16, 3), (19, 18), (22, 3),
     (23, 13), (29, 0)]
    >>> list(skyline([(1, 3, 2), (1, 3, 2)]))
    [(1, 3), (2, 0)]

    """
    # Map from x-coordinate to event.
    events = defaultdict(lambda: Event(start=[], end=[]))
    for left, height, right in buildings:
        b = Building(height)
        events[left].start.append(b)
        events[right].end.append(b)

    standing = []  # Heap of buildings currently standing.
    last_h = 0  # Last emitted skyline height.

    # Process events in order by x-coordinate.
    for x, event in sorted(events.items()):
        # Update buildings currently standing.
        for b in event.start:
            heappush(standing, b)
        for b in event.end:
            b.finished = True

        # Pop any finished buildings from the top of the heap.
        while standing and standing[0].finished:
            heappop(standing)

        # Top of heap (if any) is the highest standing building, so
        # its height is the current height of the skyline.
        h = standing[0].height if standing else 0

        # Yield co-ordinates if the skyline height has changed.
        if h != last_h:
            yield x, h
            last_h = h

    from timeit import timeit
    import random
    R = lambda n: random.randrange(n) + 1
    buildings = sorted((x, R(10000), x + R(1000)) for x in (R(10000) for _ in range(50000)))
    timeit(lambda: list(skyline(buildings)), number=1)


def product(nums):
    from functools import reduce
    from operator import mul
    return reduce(mul, nums)


def max_product(numbers):
    from heapq import heappushpop

    positive = [0, 0, 0]
    negative = [0, 0]
    for x in numbers:
        if x > positive[0]:
            heappushpop(positive, x)
        elif x < 0 and -x > negative[0]:
            heappushpop(negative, -x)

    return max(product(positive[-1:] + negative), product(positive))


def _num_of_changes(amount, denominations, i=0, combinations=[]):
    if amount == 0:
        return 1
    elif amount < 0:
        return None
    else:
        s = 0
        for i in range(i, len(denominations)):
            coin = denominations[i]
            if amount - coin < coin:
                c = _num_of_changes(amount - coin, denominations, i + 1)
            else:
                c = _num_of_changes(amount - coin, denominations, i)

            if c:
                s += c
        return s


def num_of_changes(amount, denominations):
    return _num_of_changes(amount, denominations)


print(num_of_changes(4, [1, 2, 3]))

# TrieNode.

import collections.abc


class TrieNode(collections.abc.MutableMapping):
    def __init__(self, k=None):
        self._data = {}
        self._value = k
        self.end = False

    def build_data(self, k):
        self[k] = True

    @staticmethod
    def build(iterable):
        root = TrieNode(None)
        for key in iterable:
            root[key] = True
        return root

    @property
    def value(self):
        return self._value

    def contains(self, item):
        def __contains__(key):
            try:
                x = self[key]
            except KeyError:
                return False
            return x.end

        return __contains__(item)

    def _keys(self, key):
        partial = ''
        for k in key:
            partial += k
            yield k, partial

    def _walk(self, data, key, *, build=False):
        if not key:
            raise ValueError()

        node = data
        if not build:
            for k in key[:-1]:
                node = node._data[k]
        else:
            for k, key_ in self._keys(key[:-1]):
                node = node._data.setdefault(k, TrieNode(key_))
        return key[-1], node

    def __getitem__(self, key):
        key, node = self._walk(self, key)
        return node._data[key]

    def __setitem__(self, key, value):
        k, node = self._walk(self, key, build=True)
        node = node._data.setdefault(k, TrieNode(key))
        node.end = value

    def __delitem__(self, key):
        key, node = self._walk(self, key)
        del node._data[key]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)


def word_break(word_string, words):
    words = TrieNode.build(words)
    output = {0: [[]]}
    partials = []
    for i, k in enumerate(word_string, 1):
        new_partials = []
        for partial in partials + [words]:
            partial = partial.get(k)
            if partial is None:
                continue

            new_partials.append(partial)
            if not partial.end:
                continue

            val = partial.value
            prevs = output.get(i - len(val))
            if prevs is not None:
                output.setdefault(i, []).extend([p + [val] for p in prevs])
        partials = new_partials
    return output[len(word_string)]


words = word_break('catsanddog', {'cat', 'cats', 'sand', 'and', 'dog'})
print(words)

trieNode = TrieNode(None)
for word in {'cat', 'cats', 'sand', 'and', 'dog'}:
    trieNode.build_data(word)

for word in {"cat", "do", "dog", "and", 'sands', 'sand'}:
    if trieNode.contains(word):
        print(word)


def sockMerchant(arr, n):
    from collections import defaultdict
    pairs_dict = defaultdict(lambda: 0)
    for v in arr:
        pairs_dict[v] += 1

    pairs = 0
    for k, v in pairs_dict.items():
        pairs += int(v / 2)

    print(f"No of pairs: {pairs}")


def counting_valleys(steps):
    step_up = 1
    step_down = -1
    sea_level = 0
    step_map = {'U': step_up, 'D': step_down}
    current_level = sea_level
    valley_start = False
    valley_end = False
    valley_count = 0

    for index, step in enumerate(steps, start=1):
        if current_level == sea_level and step == 'D':
            valley_start = True
        elif current_level == -1 and step == 'U':
            valley_end = True
            valley_count += 1
        current_level += step_map[step]

    return valley_count


def jumpingOnClouds(c):
    def get_three(i):
        first, *rest = c[i:]
        if len(rest) >= 2:
            second, third, *_ = rest
            return first, second, third
        elif len(rest) == 1:
            second = rest
            return first, second, None
        return first, None, None

    jumps = 0
    index = 0
    while True:
        cur, next_item, next_to_next = get_three(index)
        if next_to_next is not None and next_to_next == 0:
            index += 2
        else:
            index += 1
        jumps += 1
        if index + 1 >= len(c):
            break
    return jumps


def compareTriplets(a: List[int], b: List[int]):
    result = [0, 0]

    for x, y in zip(a, b):
        if x < y:
            result[1] += 1
        elif x > y:
            result[0] += 1

    return result


def happines_index(set_a, set_b, arr: List[int]):
    n, m = [int(i) for i in input().strip('\'\n\"').split()]
    set_a = set(map(int, input().strip('\'\n\"').split()))
    set_b = set(map(int, input().strip('\'\n\"').split()))

    arr = list(map(int, input().strip('\'\n\"').split()))

    happiness = sum((i in set_a) - (i in set_b) for i in arr)

    print(happiness)
    return happiness


