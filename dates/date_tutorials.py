from datetime import datetime, timedelta


def today():
    return datetime.now()


def yesterday():
    one_day = timedelta(days=1)
    return today() - one_day


def add_days(num_of_days):
    days = timedelta(days=num_of_days)
    return today() + days


def prev_days(num_of_days):
    return add_days(-num_of_days)


def print_dates():
    print(f"\n\nFollowing the tutorial of dates. Printing dates..")
    today = datetime.now()
    one_day = timedelta(days=1)
    yesterday = today - one_day

    print(f'Today = {today}, Yesterday = {yesterday}\n')

    # print date , month, year, hour, min, sec.
    print(
        f'Date:{today.day}, Month:{today.month}, Year:{today.year}, Hour:{today.hour}, Minute:{today.minute}, Second:{today.second}')

    print("parsing date with the format, dd/mm/yyyy")
    birthdate = datetime.strptime("14/07/2012", "%d/%m/%Y")
    print(f'Karthik\'s birthday: {birthdate}')


print_dates()