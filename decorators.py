# This file introduces decortors.
# Write a function, decorate a logger type function around it


def traceFun(fun):
    """ This trace method is used to induce traces into the functions
    :param fun:
    :return:
    """

    def wrapper(*args, **kwargs):
        print(f'Calling {fun.__name__}' f' with {args}, {kwargs}')
        original_result = fun(*args, **kwargs)
        print(f"{fun.__name__} returned {original_result}")
        return original_result

    return wrapper


@traceFun
def printNum(num):
    """ This method is used to print the num that's passed to it
    :param num:
    """
    print(num)
