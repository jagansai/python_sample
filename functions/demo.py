def average(*args):
    """ Demo to show passing variable number of args to the function """
    return sum(args) / len(args)


def average_test():

    assert(average(1, 2, 3, 4, 5) == 3.0)
    assert (average(*(range(1, 6))) == 3.0)

