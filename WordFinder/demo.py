from collections import namedtuple
from concurrent import futures
from itertools import permutations
from multiprocessing import cpu_count
from time import time
from typing import List, Dict

from HackerRank.problems import TrieNode as Trie
#from Tries.demo import Trie

from collections_demo.demo import HashDict

# from datrie import Trie
WordAndWordPair = namedtuple("WordAndWordPair", ["orig_word", "first", "second"])
default_word_pair = WordAndWordPair(orig_word="", first="", second="")


class WordFinder(object):
    def __init__(self, orig_word, word_cache: Trie):
        self.orig_word = orig_word
        self.word_cache = word_cache

    def get_words_from_word(self, word_size, found_words_dict: Dict[int, List[str]]):

        def find_word_pairs(word_finder: WordFinder, current_len) -> (bool, WordAndWordPair):
            for word in found_words_dict[current_len]:
                remaining_char_in_word = remove_letter(word_finder.orig_word, word)
                remaining_char_in_word = "".join(sorted(remaining_char_in_word)).lower()
                pair_words_list = found_words_dict[len(remaining_char_in_word)].copy()
                for next_word in pair_words_list:
                    tmp_word = "".join(sorted(next_word)).lower()
                    if tmp_word == remaining_char_in_word:
                        return True, WordAndWordPair(word_finder.orig_word, word, next_word)

            return False, default_word_pair

        def get_words_impl(word_finder: WordFinder):
            try:
                for word in get_permutations(word_finder.orig_word, word_size):
                    if word_finder.word_cache.contains(word):
                        found_words_dict[len(word)].append(word)
                        found, word_pair = find_word_pairs(word_finder, len(word))
                        if found:
                            return found, word_pair
            except Exception:
                pass
            return False, WordAndWordPair(word_finder.orig_word, "", "")

        return get_words_impl(self)


# Helper methods for getting permutation.
def get_permutations(word, min_word_size=2):
    char_list = [c for c in word]
    for c in permutations(char_list, min_word_size):
        yield "".join(c)


# Helper method to remove all the characters from the word ( only first occurrence )
def remove_letter(word: str, chars_to_remove):
    for c in chars_to_remove:
        word = word.replace(c, '', 1)
    return word


def get_words(word_finder: WordFinder, timeout=None, min_word_size=2):
    # define another method that does the real finding job.
    # define a default dictionary with keys and values are list.
    found_words_dict: Dict[int, List[int]] = {}

    for i in range(min_word_size, len(word_finder.orig_word) + 1):
        found_words_dict[i] = []

    with futures.ThreadPoolExecutor() as pool:
        future_list = []
        try:
            for i in range(min_word_size, len(word_finder.orig_word) + 1):
                future_list.append(pool.submit(word_finder.get_words_from_word, i, found_words_dict))

                for future in futures.as_completed(future_list):
                    if future.result() is not None:
                        found, word_pair = future.result()
                        if not found:
                            continue
                        else:
                            return found, word_pair
        except Exception:
            pass
        finally:
            # this is not a graceful stopping of threads.
            # In the absence of graceful option, this is the only way.
            pool._threads.clear()
            futures.thread._threads_queues.clear()

    return False, WordAndWordPair(word_finder.orig_word, "", "")


def find_words(filename):
    # Define a map that loads all the words.
    word_cache = Trie()
    with open(filename, mode='r') as f:
        for word in f:
            word = word.strip('\n')
            if all(c.isalpha() for c in word):
                word_cache.build_data(word.strip('\n'))

    print(f"loaded data into cache")

    words = ["Golyrbi", "Gondylou", "WpnuOd", "Naawmnom", "Titlghfer", "Egmooc", "Ydignhat", "Yyiannnurs",
             "Ciiavelglyt"]

    start_time = time()
    iterations = 0
    with futures.ThreadPoolExecutor(cpu_count()) as executor:
        future_list = []
        for word in words:
            future_list.append(executor.submit(get_words, WordFinder(word, word_cache), timeout=None))

        for future in futures.as_completed(future_list):
                found, word_pair = future.result()
                if found:
                    print(f"{word_pair.orig_word} -> [{word_pair.first}, {word_pair.second}]")

    print(f"Took {time() - start_time} seconds")