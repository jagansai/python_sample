# Demo for positional and enforcing positional arguments.
from datetime import datetime


def safe_division(number, divisor, *,
                  ignore_overflow=False,
                  ignore_division=False):
    try:
        return number / divisor
    except OverflowError:
        if ignore_overflow:
            return 0
        else:
            raise
    except ZeroDivisionError:
        if ignore_division:
            return float('inf')
        else:
            raise


def log(message, when=None):
    """
    :param message: Message to print
    :type message: str
    :param when: datetime of when the message occured. Defaults to the present time.
    :return: None
    """
    if when is None:
        when = datetime.now()
    print(f"{when}: {message}")
