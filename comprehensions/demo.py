from math import sqrt


def list_comprehension():
    # simple example.
    for i in range(5):
        print(sqrt(i))

    print("Now use list comprehension")
    [sqrt(i) for i in range(5)]

    # Filtering list.
    print([i for i in range(5) if i % 2 == 0])


def fibonacci_demo():
    def fibonacci():
        yield 1
        yield 1
        arr = [1, 1]
        while True:
            arr = [arr[-1], sum(arr[-2:])]
            yield arr[-1]

    for i in fibonacci():
        if i > 100:
            break
        print(i)


def dict_comprehensions():
    SPECIES = "whale", "grasshopper", "lizard"
    CLASS = "mammal", "insect", "reptile"

    d = {}
    print("Using normal loop with zip")
    for species, class_ in zip(SPECIES, CLASS):
        d[species.capitalize()] = class_.capitalize()
    print(d)

    print("Now with dict comprehension")
    d = {species.capitalize(): class_.capitalize() for species, class_ in zip(SPECIES, CLASS)}
    print(d)

    d = {
        species.capitalize(): class_.capitalize()
        for species, class_ in zip(SPECIES, CLASS)
        if class_ != "insect"
    }
    print(d)
