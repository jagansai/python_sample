# %%
class Person(object):
    """
        Person is a parent class to Employee class.
    """

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name}, {self.age}"


class Employee(Person):
    def __init__(self, name, age, sal):
        super(Employee, self).__init__(name=name, age=age)
        self.sal = sal

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, age):
        if age == 0:
            raise ValueError("Age can't be 0")
        self._age = age

    def __str__(self):
        return f"{self.name}, {self.age}, {self.sal}"


#%%
