from collections import namedtuple, OrderedDict, defaultdict
from collections import Set

def get_people():
    return [{'first': 'sai', 'last': 'pantula'},
            {'first': 'kartikeya', 'last': 'pantula'},
            {'first': 'neeraja', 'last': 'pantula'},
            {'first': 'saketh', 'last': 'pantula'}]


def collections_demo():
    people = get_people()

    rama = {'first': 'rama', 'last': 'pantula'}
    people.append(rama)

    print(f"Printing people  --> \n {people}")

    print(f"Printing first 3 items in people --> \n {people[1:4]}")
    print(f"Printing last 3 items in people --> \n {people[-3:]}")
    print(f"Printing from 2nd, all the items in people --> \n {people[1:]}")
    print(f"Printing from last but one to first in reverse order --> \n {people[len(people) - 2::-1]}")


def tuple_demo():
    t = ('Sai', 40)
    t = t + ('neeraja',)
    print(f"Printing tuple:{t}")

    print(f"check if SaiJ in {t}: {'SaiJ' in t}")


def key_values_demo():
    people = get_people()
    print(people[0])
    people.append({"first": "rama", "last": "pantula"})

    d = {
        "Mosquito": "Insect",
        "Whale": "Mammal",
        "Lizard": "Reptile"
    }

    for species, class_ in d.items():
        print(species, class_)


def set_demo():
    mammals = {"whale", "dog", "whale", "cat", "human"}
    print(mammals)

    pets = {"dog", "cat", "goldfish"}
    print(pets)

    print(mammals & pets)


def named_tuple():
    Person = namedtuple("Person", ["name", "age", "gender"])
    sai = Person(name="sai", age=40, gender="M")
    print(f"Person(name=\"{sai.name}\", age={sai.age}, gender=\"{sai.gender}\")")


def ordered_dict():
    d = OrderedDict([
        ("Lizard", "Reptile"),
        ("Whale", "Mammal")
    ])

    for species, class_ in d.items():
        print(f"{species} is a {class_}")


# Creating DefaultValue
class DefaultValue(object):
    def __init__(self, defaultValue):
        self.defaultValue = defaultValue
        self.missing = 0

    def __call__(self):
        self.missing += 1
        return self.defaultValue


def default_dict():
    fav_prog_languages = {
        "Sai": "C++",
        "Rama": "SAP",
        "Nath": "Java"
    }
    names = ["Neeraja", "Padmaja", "Rajya Lakshmi"]
    # define a default dict.
    defaultValue = DefaultValue("Python")
    d = defaultdict(defaultValue)
    d.update(fav_prog_languages)

    for name in fav_prog_languages.keys():
        names.append(name)

    for name in names:
        print(f"{name} -> {d[name]}")

    print(f"The number of missing items in the dictionary: {defaultValue.missing}")


# %%
def sort_demo():
    def get_names():
        return ["Sai", "Jagan", "Rama", "Neeraja"]

    def sort_using_lambda():
        names = get_names()
        names.sort(key=lambda x: len(x))
        print(names)

    sort_using_lambda()


# %%

# Wrapping a dictionary under a class
class HashDict(object):
    def __init__(self):
        self.tree = set()

    def search(self, key):
        key = str(key).lower()
        return key in self.tree

    def insert(self, key):
        key = str(key)
        self.tree.add(key)
