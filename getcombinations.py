from itertools import combinations
from itertools import permutations


def get_combinations(word: str):
    for i in range(1, len(word) + 1):
        for c in combinations(word, i):
            yield c


def print_word_combinations(word):
    for c in get_combinations(word):
        print(c)


def get_permutations(word: str):
    for i in range(1, len(word) + 1):
        for c in permutations(word, i):
            yield c


def print_word_permutations(word: str):
    for c in get_permutations(word):
        print(c)
