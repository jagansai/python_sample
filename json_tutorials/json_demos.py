from api.call_api import calling_api_demo
from html import unescape


def json_demo():
    results = calling_api_demo(False)
    for item in results["items"]:
        title = unescape(item["title"])
        link = unescape(item["link"])
        print(f"Title:{title}, Link:{link}\n")
